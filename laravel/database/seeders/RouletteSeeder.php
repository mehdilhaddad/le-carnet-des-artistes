<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RouletteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roulettes')->insert([
            'sujet' => 'Demain je vais dormir chez mon meilleur amis.',            
        ]);

        DB::table('roulettes')->insert([
            'sujet' => 'Un petit chat mange ses croquettes.',            
        ]);

        DB::table('roulettes')->insert([
            'sujet' => 'Les animaux de la jungle Africaine.',            
        ]);

        DB::table('roulettes')->insert([
            'sujet' => 'Les lutins de la fôret de Brocéliande.',            
        ]);

        DB::table('roulettes')->insert([
            'sujet' => ' Le mythe de la bête du Gévaudan.',            
        ]);
    }
}
